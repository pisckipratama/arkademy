-- create table category and insert data
CREATE TABLE Category(
  id INT(4) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  name VARCHAR(10) NOT NULL
);

INSERT INTO Category (name) VALUES ('Food'), ('Drink');

-- create table Cashier and insert data
CREATE TABLE Cashier(
  id INT(4) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  name VARCHAR(26) NOT NULL
);

INSERT INTO Cashier (name) VALUES ('Pevita Pearce'), ('Raisa Andriana'), ('Piscki Pratama');

-- create table Product and insert data
CREATE TABLE Product(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  name VARCHAR(25) NOT NULL,
  price INT(10) NOT NULL,
  id_category INT NOT NULL,
  id_cashier INT NOT NULL,
  FOREIGN KEY (id_category) references Category(id),
  FOREIGN KEY (id_cashier) references Cashier(id)
);

INSERT INTO Product (name, price, id_category, id_cashier) VALUES
('Latte', 10000, 2, 1), 
('Cake', 20000, 1, 2),
('Gepuk', 45000, 1, 3);

-- Menampilkan Query
SELECT Cashier.name AS cashier, Product.name AS product, Category.name as category, FORMAT(Product.Price, 0, 'id_ID') AS price 
FROM Product
INNER JOIN Category ON Product.id_category = Category.id
INNER JOIN Cashier ON Product.id_cashier = Cashier.id;