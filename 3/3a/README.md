## Jawaban Soal No. 3a
<hr>

### Capture

##### Table Product
<img src="capture/ss_1.png" width=70%>

##### Table Category
<img src="capture/ss_2.png" width=70%>

##### Table Category
<img src="capture/ss_3.png" width=70%>

##### Query 
<img src="capture/ss_4.png" width=70%>

### Cara Menjalankan
Cara menjalankan, pastikan di PC/Laptop anda sudah terinstall MySQL. 
Setelah itu buat database dengan nama <b>CRUD_PISCKI</b>.
Lalu masukkan script query yang ada di file ```3a.sql```.

### Teknologi yang dipakai
MySQL