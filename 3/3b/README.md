## Jawaban Soal No. 3b
<hr>

### Capture 

##### Home Page
<img src="images/1_home.png" width=70%>

###### Add
<img src="images/2_add.png" width=70%>

###### Edit
<img src="images/3_edit.png" width=70%>

###### Delete 
<img src="images/4_delete.png" width=70%>

### Cara Menjalankan
Buka file ```index.html``` di browser

### Teknologi yang dipakai
HTML, CSS, Bootstrap