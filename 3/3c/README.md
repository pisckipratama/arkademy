## Jawaban Soal No. 3c
<hr>

### Capture 

##### Home Page
<img src="public/images/1_home.png" width=70%>

###### Add
<img src="public/images/2_add.png" width=70%>

###### Edit
<img src="public/images/3_edit.png" width=70%>

###### Delete 
<img src="public/images/4_delete.png" width=70%>

### Cara Menjalankan
- Pastikan di Laptop/PC sudah terpasang NodeJS, NPM, dan MySQL
- Lalu pastikan di MySQL sudah ada database dengan nama <b>CRUD_PISCKI</b> dan juga pastikan table dan isinya sudah dimasukkan, jika belum bisa dilihat difile ```3a/3a.sql```.
- Setelah database dan table sudah diinsert lalu setting database connection di folder ```config/connection.db``` sesuaikan *host*, *user*, dan *password*.
- Lalu masukkan perintah ```npm start```
- Buka alamat ```localhost:3000``` di browser.


### Teknologi yang dipakai
HTML, CSS, Bootstrap, NodeJS, MySQL, EJS.