const mysql = require('mysql2');

module.exports = {
  db: {
    get: mysql.createConnection({
      host: "localhost",
      user: "root",
      password: "", // pastikan ganti passwordnya jika berbeda
      database: "CRUD_PISCKI",
      port: "3306"
    })
  }
};