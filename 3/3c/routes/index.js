var express = require('express');
var router = express.Router();
var mysql = require('mysql2');
var config = require('../config/connection');

var connection = config.db.get;

/* GET home page. */
router.get('/', function (req, res) {
  let sqlAll = `SELECT Product.id, Cashier.name AS cashier, Product.name AS product, Category.name as category, FORMAT(Product.Price, 0, 'id_ID') AS price FROM Product INNER JOIN Category ON Product.id_category = Category.id INNER JOIN Cashier ON Product.id_cashier = Cashier.id`;
  let sqlCashier = `SELECT * FROM Cashier`;
  let sqlCategory = `SELECT * FROM Category`;

  connection.query(sqlAll, (err, resultAll) => {
    if (err) return res.render('error', { message: err.message, err });

    connection.query(sqlCashier, (err, resultCashier) => {
      if (err) return res.render('error', { message: err.message, err });

      connection.query(sqlCategory, (err, resultCategory) => {
        if (err) return res.render('error', { message: err.message, err });
        return res.render('index', { title: 'Tes Arkademy', resultAll, resultCashier, resultCategory });
      });
    });
  });
});

/* POST Data */
router.post('/', function (req, res) {
  console.log(req.body);
  const { cashier, category, product, price } = req.body;
  let insertSQL = `INSERT INTO Product (name, price, id_category, id_cashier) VALUES (?, ?, ?, ?)`;
  connection.query(insertSQL, [product, parseInt(price), parseInt(category), parseInt(cashier)], (err) => {
    if (err) return res.render('error', { message: err.message, err });
    res.redirect('/');
  });
});

/* GET Landing Edit Page */
router.get('/edit/:id', function (req, res) {
  let sql = `SELECT * FROM Product WHERE id=?`;
  let sqlCashier = `SELECT * FROM Cashier`;
  let sqlCategory = `SELECT * FROM Category`;

  connection.query(sqlCategory, (err, resultCategory) => {
    if (err) return res.render('error', { message: err.message, err });

    connection.query(sqlCashier, (err, resultCashier) => {
      if (err) return res.render('error', { message: err.message, err });

      connection.query(sql, [req.params.id], (err, resultOne) => {
        if (err) return res.render('error', { message: err.message, err });
        let dataProduct = resultOne[0];
        return res.render('edit', { title: 'Tes Arkademy', dataProduct, resultCashier, resultCategory });
      });
    });
  });
});

router.post('/edit/:id', function (req, res) {
  const { cashier, category, product, price } = req.body;
  let sql = `UPDATE Product SET name=?, price=?, id_category=?, id_cashier=? WHERE id=?`;
  connection.query(sql, [product, parseInt(price), parseInt(category), parseInt(cashier), parseInt(req.params.id)], err => {
    if (err) return res.render('error', { message: err.message, err });
    res.redirect('/');
  });
});

router.get('/delete/:id', function (req, res) {
  let sql = `DELETE FROM Product WHERE id=?`;
  connection.query(sql, [req.params.id], (err) => {
    if (err) return res.render('error', { message: err.message, err });
    res.redirect('/');
  })
})

module.exports = router;

